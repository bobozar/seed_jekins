from jenkinsapi.jenkins import Jenkins
import datetime
import sqlite3

conn = sqlite3.connect('starsdb.db')
cur_obj = conn.cursor()

def kick_off_server_instance():
    jek_url = 'http://jenkins_host:8080'
    server = Jenkins(jek_url, username = 'bobo', password = 'mysurname')
    return server


def tap_job_details():
    """ after taking the data, lets assume a table called JEKIN_DATA has been created with the fields 'status' and 'date'
        in the DB starsdb
    """
    main_server = kick_off_server_instance()
    for myinstance in main_server.get_jobs():
        stat = myinstance.is_running()
        time_checked = datetime.datetime.now()
        cur_obj.execute("INSERT INTO JEKIN_DATA VALUES ('?,?')", (stat, time_checked))
    conn.commit()
    conn.close()
